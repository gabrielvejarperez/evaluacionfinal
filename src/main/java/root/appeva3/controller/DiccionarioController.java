/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.appeva3.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.appeva3.dao.ConsultasJpaController;
import root.appeva3.dto.PalabraDTO;
import root.appeva3.dto.oxford.Palabra;
import root.appeva3.entity.Consultas;

/**
 *
 * @author gveja
 */
@WebServlet(name = "DiccionarioController", urlPatterns = {"/DiccionarioController"})
public class DiccionarioController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DiccionarioController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DiccionarioController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        PalabraDTO palabraDTO = new PalabraDTO();
        String palabra = request.getParameter("palabra");
        try {

            Client cliente = ClientBuilder.newClient();
            WebTarget recurso = cliente.target("https://od-api.oxforddictionaries.com:443/api/v2/entries/es/" + palabra.toLowerCase());
            Palabra palabraResp = recurso.request(MediaType.APPLICATION_JSON).header("app_id", "075eaa02").header("app_key", "679deb85a9389375517dc28fd6b852b2").get(Palabra.class);
            String definicion = palabraResp.getResults().get(0).getLexicalEntries().get(0).getEntries().get(0).getSenses().get(0).getDefinitions().get(0);

            palabraDTO.setPalabra(palabra);
            palabraDTO.setSignificado(definicion);
            
            ConsultasJpaController dao = new ConsultasJpaController();
            SimpleDateFormat fecha = new SimpleDateFormat("yyyy.MM.dd HH.mm.ss");
            
            Date date = new Date();
            String fechas = new Timestamp(date.getTime()).toString();
            
            Consultas consulta = new Consultas(palabra, definicion, fechas);
            dao.create(consulta);
            
        } catch (NotFoundException notFoundException) {
            palabraDTO.setPalabra(palabra);
            palabraDTO.setSignificado("PALABRA NO ENCONTRADA");
        }  catch (Exception ex) {
        }
    
        request.setAttribute("palabraDTO", palabraDTO);
        request.getRequestDispatcher("resultado.jsp").forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}