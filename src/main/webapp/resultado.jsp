<%-- 
    Document   : resultado
    Created on : 23-10-2021, 1:30:11
    Author     : gveja
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="root.appeva3.dto.PalabraDTO"%>
<!DOCTYPE html>

<%
    PalabraDTO pal = (PalabraDTO) request.getAttribute("palabraDTO");

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Significado de <%= pal.getPalabra() %></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    </head>
    <body>
        <div style="padding: 16px; background-color: lightgray">
            <a class="btn btn-primary" href="index.jsp" role="button">Nueva Consulta</a>
            <a class="btn btn-primary" href="listar.jsp" role="button">Ver consultas realizadas</a>
        </div>
        
        <div style="padding: 16px;">
        <h1><%= pal.getPalabra().toUpperCase() %> </h1>
        <p style="font-size: 28px"><b>Significado:</b>   <%= pal.getSignificado()%> </p>

        </div>

    </body>
</html>
