<%-- 
    Document   : listar
    Created on : 15-10-2021, 21:41:23
    Author     : gveja
--%>

<%@page import="root.appeva3.dao.ConsultasJpaController"%>
<%@page import="root.appeva3.entity.Consultas"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    ConsultasJpaController dao = new ConsultasJpaController();
    List<Consultas> lista = (List<Consultas>) dao.findConsultasEntities();
    Iterator<Consultas> itLista = lista.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Palabras consultadas</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    </head>
    <body>
        <div style="padding: 16px; background-color: lightgray">
            <a class="btn btn-primary" href="index.jsp" role="button">Nueva Consulta</a>
            <a class="btn btn-primary" href="listar.jsp" role="button">Ver consultas realizadas</a>
        </div>
        <div style="padding: 16px">
            <h1>Lista de palabras consultadas</h1>

            <table class="table table-hover">
                <thead>
                    <th>Palabra</th>
                    <th>Significado </th>
                    <th>Fecha de consulta </th>
                </thead>
                <tbody>

                    <% while (itLista.hasNext()) {
                            Consultas consultas = itLista.next();%>
                    <tr>
                        <td><%=consultas.getPalabra() %></td>
                        <td><%=consultas.getSignificado()%></td>
                        <td><%=consultas.getFecha()%></td>
                    </tr>
                    <%}%>
                </tbody>
            </table>
        </div>
    </body>
</html>
