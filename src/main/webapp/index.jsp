<%-- 
    Document   : index
    Created on : 22-10-2021, 23:39:03
    Author     : gveja
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nueva consulta</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    </head>
    <body>
        <div style="padding: 16px; background-color: lightgray">
            <a class="btn btn-primary" href="index.jsp" role="button">Nueva Consulta</a>
            <a class="btn btn-primary" href="listar.jsp" role="button">Ver consultas realizadas</a>
        </div>

        <div style="padding: 16px;">
            <form  name="form" action="DiccionarioController" method="POST">
                <input class="form-control" type="text" name="palabra" id="palabra" placeholder="Ingrese palabra a buscar" style="margin-bottom: 16px">

                <input class="btn btn-primary" type="submit" name="buscar"  value="Consultar">     
            </form>
        </div>

        

    </body>
</html>
